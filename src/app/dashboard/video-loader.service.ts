import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, combineLatest } from 'rxjs';

import { Video } from './types';
import { ActivatedRoute, Router } from '@angular/router';
import { map, filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VideoLoaderService {

  private _videoList: Video[] = [];
  private videoList = new BehaviorSubject<Video[]>(this._videoList);

  get videoList$() {
    return this.videoList.asObservable();
  }


  private _selectedVideo: Video;
  selectedVideo = new BehaviorSubject<Video>(this._selectedVideo);

  get selectedVideo$() {
    return this.selectedVideo.asObservable();
  }

  constructor(private http: HttpClient, route: ActivatedRoute, private router: Router) {
    this.http.get<Video[]>('https://api.angularbootcamp.com/videos')
      .subscribe(videos => {
        this._videoList = videos;
        this.videoList.next(this._videoList);
      });

    const selectedId: Observable<string> = route.queryParams
      .pipe(
        map(params => params.selected)
      );

    combineLatest(this.videoList, selectedId)
      .pipe(
        filter(combined => !!(combined[0] && combined.length)),
        map(combined => {
          const videosAry = combined[0];
          const id = combined[1];

          if (!id) {
            return videosAry[0];
          }

          const selected = videosAry.find((v: Video) => v.id === id);
          return selected ? selected : videosAry[0];
        })
      )
      .subscribe(selectedVideo => {
        this._selectedVideo = selectedVideo;
        this.selectedVideo.next(this._selectedVideo);
      });
  }

  makeSelection(id: string) {
    this.router.navigate([], {
      queryParams: {selected: id},
      queryParamsHandling: 'merge'
    });
  }

}
