import { Component } from '@angular/core';

import { Video } from '../types';
import { VideoLoaderService } from 'src/app/dashboard/video-loader.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent {

  videos: Observable<Video[]>;
  selectedVideo: Observable<Video>;

  constructor(videoSvc: VideoLoaderService) {
    this.videos = videoSvc.videoList$;
    this.selectedVideo = videoSvc.selectedVideo$;
  }
}
