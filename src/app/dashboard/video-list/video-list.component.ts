import { Component, Input} from '@angular/core';
import { Video } from '../types';
import { VideoLoaderService } from '../video-loader.service';



@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent {

  @Input() videos: Video[];
  @Input() selected: Video;

  constructor(private videoSvc: VideoLoaderService) {
  }

  onClick(id: string) {
    this.videoSvc.makeSelection(id);
  }

}
